const APP_CONFIGS = {
    buildingName: "ЖК «Серебрица»",
    buildingLatLng: {
        lat: 55.638434,
        lng: 37.857576
    },
    gmapsStyles: [{
        "featureType": "administrative.land_parcel",
        "elementType": "all",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [{
            "color": "#f2f2f2"
        }]
    }, {
        "featureType": "landscape.man_made",
        "elementType": "all",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [{
            "visibility": "simplified"
        }, {
            "lightness": 20
        }]
    }, {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [{
            "color": "#fcb943"
        }]
    }, {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [{
            "color": "#ffedcc"
        }]
    }, {
        "featureType": "road.highway",
        "elementType": "labels",
        "stylers": [{
            "visibility": "simplified"
        }]
    }, {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [{
            "hue": "#ffcb00"
        }, {
            "weight": "5.00"
        }]
    }, {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#bfc9c8"
        }, {
            "weight": "3.00"
        }]
    }, {
        "featureType": "road.arterial",
        "elementType": "labels",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [{
            "visibility": "simplified"
        }]
    }, {
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#cccfcf"
        }]
    }, {
        "featureType": "road.local",
        "elementType": "labels",
        "stylers": [{
            "visibility": "simplified"
        }]
    }, {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [{
            "visibility": "simplified"
        }]
    }, {
        "featureType": "water",
        "elementType": "all",
        "stylers": [{
            "color": "#7dcac5"
        }]
    }]
};
$.extend($.fancybox.defaults, {
    hash: false,
    infobar: true,
    buttons: ['close'],
    margin: [40, 40],
    lang: 'ru',
    transitionEffect: "slide",
    touch: false,
    animationDuration: 200,
    mobile: {
        clickContent: false,
        clickSlide: false
    },
    i18n: {
        'ru': {
            CLOSE: 'Закрыть',
            NEXT: 'Далее',
            PREV: 'Назад',
            ERROR: 'Не удалось открыть галерею - попробуйте позже',
            PLAY_START: 'Запустить слайдшоу',
            PLAY_STOP: 'Пауза',
            FULL_SCREEN: 'На весь экран',
            THUMBS: 'Миниатуры'
        }
    },
    btnTpl: {
        smallBtn: '<button data-fancybox-close class="Btn Btn-modalClose" title="{{CLOSE}}"><svg class="SvgIco" height="13" viewBox="0 0 13 13" width="13" xmlns="http://www.w3.org/2000/svg"><path class="SvgIco_path" d="m532.31 19.31h7v2h-7v7h-2v-7h-7v-2h7v-7h2z"  fill-rule="evenodd" transform="matrix(.70710678 .70710678 -.70710678 .70710678 -355.02 -383.75)"/></svg></button>'
    }

    /*afterClose: function() {
        if (this.type === "inline") {
              let $form = this.$content.find('form:first');
            let validator = $form.trigger('reset').data("validator");
            if (validator) {
                validator.resetForm();
                $form.find('input.error').removeClass('error');
            }
              let $successText = this.$content.find('.b-form-success:first');
            if ($successText.length) {
                $form.parent().show();
                $successText.remove();
            }
        }
    }*/
});

class Fitblock {
    constructor(el) {
        this.outer = el;
        this.$el = $(el);
        this.inner = el.getElementsByClassName('Fitblock_inner')[0];
        this.pusher = this.inner.getElementsByClassName('Fitblock_pusher')[0];

        $(window).on('resize.fitblocks orientationchange.fitblocks', _.debounce(() => {
            this.refresh();
        }, 100));

        setTimeout(() => {
            this.refresh();
        }, 100);
    }

    refresh() {

        let outer = this.outer;
        let inner = this.inner;
        let pusher = this.pusher;
        let wrapH = outer.offsetHeight;
        let wrapW = outer.offsetWidth;

        let pusherH = pusher.naturalHeight;
        let pusherW = pusher.naturalWidth;

        let rel = pusherW / pusherH;
        if (wrapW / pusherW > wrapH / pusherH) {
            pusher.style.width = `${wrapW}px`;
            pusher.style.height = "auto";
            inner.style.marginLeft = `-${wrapW / 2}px`;
            inner.style.marginTop = `-${wrapW / rel / 2}px`;
        } else {
            pusher.style.width = "auto";
            pusher.style.height = `${wrapH}px`;
            inner.style.marginLeft = `-${wrapH * rel / 2}px`;
            inner.style.marginTop = `-${wrapH / 2}px`;
        }

        inner.style.visibility = "visible";
    }

    destroy() {
        $(window).off('.fitblocks');
    }
}
class Preloader {
    constructor(selector) {

        this.el = document.querySelector(selector);
        /*if (!el) {
            console.error("Preloader class constructor cant't find target element!");
            return;
        }*/
        this.el.onclick = event => {
            event.preventDefault();
            event.stopPropagation();
            return false;
        };

        this.isLocked = false;
    }

    /*lock() {
        this.isLocked = true;
    }
      unlock() {
        this.isLocked = false;
    }*/

    show(cb) {
        if (this.isLocked) return false;
        this.el.classList.add("Preloader-active");
        if (cb) cb();
    }

    hide(cb) {
        if (this.isLocked) return false;
        this.el.classList.remove("Preloader-active");
        if (cb) cb();
    }

}

const currentDevice = device.default;
const $window = $(window);
const $document = $(document);
const $body = $('body');
const $header = $('.header');
const active = '_active';
const categories = '_categories';

$window.on('load', () => {
    window.pagePreloader = new Preloader("#pagePreloader");
    window.pagePreloader.hide();

    (function ($forms) {
        if (!$forms.length) return;

        let curWidth = $document.width();

        if (curWidth < 690) {
            $forms.each(function () {
                let $form = $(this);
                let $inputs = $form.find('.form__inp');

                $inputs.each(function () {
                    let $input = $(this);
                    let placeholder = $input.attr('placeholder');

                    $input.attr('placeholder', '').before('<em>' + placeholder + '</em>');
                });
            });
        }

        console.log($document.width());
    })($('form'));

    (function ($button) {
        if (!$button) {
            return;
        }

        let $close = $('[data-menu-close]');

        $close.on('click', function () {
            $body.removeClass(active);
        });

        $button.on('click', function (e) {
            e.preventDefault();

            if ($body.hasClass(active)) {
                $body.removeClass(active);

                return;
            }

            $body.addClass(active);
        });

        $document.on('click', function (e) {
            let $target = $(e.target);

            if (!$target.closest('.mobile-nav').length && !$target.is('[data-menu-toggle]')) {
                $body.removeClass(active);
            }
        });
    })($('[data-menu-toggle]'));

    $window.on('scroll', function () {
        let isFixed = $window.scrollTop() > 0 ? 'addClass' : 'removeClass';

        $body[isFixed]('_fixed');
    });

    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = $header.outerHeight();

    $window.on('scroll', function () {
        didScroll = true;
    });

    // Fixed header
    setInterval(function () {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
        var st = $(this).scrollTop();

        if (Math.abs(lastScrollTop - st) <= delta) return;

        if (st > lastScrollTop && st > navbarHeight) {
            $header.removeClass('nav-down').addClass('nav-up');
        } else {
            if (st + $window.height() < $document.height()) {
                $header.removeClass('nav-up').addClass('nav-down');
            }
        }

        lastScrollTop = st;
    }

    // slider
    $('[data-slider]').each(function () {
        let $slider = $(this);
        let $tabs = $('.slider-tabs-mob__item');

        $slider.slick({
            slidesToShow: 3,
            dots: false,
            variableWidth: false,
            infinite: false,
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    dots: true,
                    arrows: false
                }
            }]
        });

        $tabs.on('click', function (e) {
            let $this = $(this);
            let index = $this.index();

            console.log(index);

            $slider.slick('slickGoTo', index);
        });

        $slider.on('afterChange', function (event, slick, currentSlide, nextSlide) {
            console.log(currentSlide);

            $tabs.removeClass(active).eq(currentSlide).addClass(active);
        });
    });

    // Custom select
    let $select = $('[data-custom-select]');

    $select.selectmenu();

    // Инициализация форм
    webshim.setOptions('waitReady', false);
    webshim.setOptions('forms', {
        lazyCustomMessages: true,
        replaceValidationUI: true,
        addValidators: true
    });

    webshim.polyfill('forms forms-ext');

    // Форматирование цены
    function int2price(sum) {
        return sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }

    //
    $('[data-ui-slider]').each(function () {
        let $slider = $(this);
        let maxVal = $slider.data('max-range');
        let minVal = $slider.data('min-range');
        let step = $slider.data('step');
        let $input = $('#calculator-input');
        let value = Number($input.val().replace(/\D/g, '').substring(0, 8));

        $slider.slider({
            range: 'min',
            max: maxVal,
            min: minVal,
            value: value,
            step: step,
            slide: function (e, ui) {
                let value = ui.value;

                $input.val(int2price(value));
                setCoefficients(value);
            }
        });

        $input.on('input', function () {
            let $this = $(this);
            let value = Number($this.val().replace(/\D/g, '').substring(0, 8));

            if (value > maxVal) value = maxVal;

            if (value <= 0) {
                $this.val('');

                return;
            }

            $this.val(int2price(value));
            setCoefficients(value);
        });

        function setCoefficients(value) {
            let $bonus = $('[data-bonus]');
            let scores = value * 0.01;

            $bonus.each(function () {
                let $this = $(this);
                let k = $this.data('bonus');
                let res = int2price(Math.floor(scores * k));

                $this.text(res);
            });
        }
    });

    $('[data-modal]').fancybox({
        padding: 0,
        btnTpl: {
            smallBtn: '<div data-fancybox-close class="popup-form__close"></div>'
        },
        helpers: {
            overlay: {
                locked: false
            }
        }
    });

    $('[data-phone]').each(function () {
        var $this = $(this);

        $this.inputmask('+7 (999) 999-99-99', {
            showMaskOnHover: false
        });
    });

    $('[data-placeholder]').on('focus', function () {
        let $this = $(this);

        $this.on('keyup', function (e) {
            let $this = $(this);

            if ($this.val() != '') {
                $this.addClass('_focus');
            } else {
                $this.removeClass('_focus');
            }

            if (e.keyCode == '13') {
                $this.blur().removeClass('_focus');

                return;
            }
        });
    });

    $('[data-arr-up]').on('click', function () {
        $('body, html').animate({
            scrollTop: 0
        }, 500);
    });

    $('[data-categories]').on('click', function (e) {
        e.preventDefault();

        $body.addClass(categories);
    });

    $('[data-categories-close]').on('click', function (e) {
        $body.removeClass(categories);
    });

    $('[data-show-more]').on('click', function (e) {
        let $this = $(this);
        let type = $this.data('show-more');
        let url = type === 'partners' ? 'partners.php' : 'news.php';

        console.log(url);

        e.preventDefault();

        $this.addClass(active);

        $.ajax({
            url: url,
            type: 'POST',
            data: '',
            dataType: 'json',
            success: function () {
                console.log('success');
            },
            complete: function (e) {
                // $this.removeClass(active);
            }
        });
    });
});
//# sourceMappingURL=common.js.map
