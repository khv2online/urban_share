class Preloader {
    constructor(selector) {
        
        this.el = document.querySelector(selector);
        /*if (!el) {
            console.error("Preloader class constructor cant't find target element!");
            return;
        }*/
        this.el.onclick = (event) => {
            event.preventDefault();
            event.stopPropagation();
            return false;
        };

        this.isLocked = false;
    }

    /*lock() {
        this.isLocked = true;
    }

    unlock() {
        this.isLocked = false;
    }*/

    show(cb) {
        if (this.isLocked) return false;
        this.el.classList.add("Preloader-active");
        if (cb) cb();
    }

    hide(cb) {
        if (this.isLocked) return false;
        this.el.classList.remove("Preloader-active");
        if (cb) cb();
    }

}