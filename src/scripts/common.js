/*=require ./includes/chunks/APP_CONFIGS.js */
/*=require ./includes/chunks/fancybox_defaults.js */
/*=require ./includes/blocks/*.js */

const currentDevice = device.default;
const $window       = $(window);
const $document     = $(document);
const $body         = $('body');
const $header       = $('.header');
const active        = '_active';
const categories    = '_categories';

$window.on('load', () => {
    window.pagePreloader = new Preloader("#pagePreloader");
    window.pagePreloader.hide();

    (function($forms) {
        if (!$forms.length) return;

        let curWidth = $document.width();

        if (curWidth < 690) {
            $forms.each(function() {
                let $form = $(this);
                let $inputs = $form.find('.form__inp');

                $inputs.each(function() {
                    let $input = $(this);
                    let placeholder = $input.attr('placeholder');

                    $input
                        .attr('placeholder', '')
                        .before('<em>' + placeholder + '</em>');
                });
            });
        }

        console.log($document.width())
    })($('form'));

    (function($button) {
        if (!$button) {
            return;
        }

        let $close = $('[data-menu-close]');

        $close.on('click', function() {
            $body.removeClass(active);
        });

        $button.on('click', function(e) {
            e.preventDefault();

            if ($body.hasClass(active)) {
                $body.removeClass(active);

                return;
            }

            $body.addClass(active);
        });

        $document.on('click', function(e) {
            let $target = $(e.target);

            if (!$target.closest('.mobile-nav').length && !$target.is('[data-menu-toggle]')) {
                $body.removeClass(active);
            }
        });
    })($('[data-menu-toggle]'));

    $window.on('scroll', function() {
        let isFixed = ($window.scrollTop() > 0) ? 'addClass' : 'removeClass';

        $body[isFixed]('_fixed');
    });

    var didScroll;
    var lastScrollTop = 0;
    var delta         = 5;
    var navbarHeight  = $header.outerHeight();

    $window.on('scroll', function() {
        didScroll = true;
    });

    // Fixed header
    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
        var st = $(this).scrollTop();

        if (Math.abs(lastScrollTop - st) <= delta)
            return;

        if (st > lastScrollTop && st > navbarHeight) {
            $header.removeClass('nav-down').addClass('nav-up');
        } else {
            if(st + $window.height() < $document.height()) {
                $header.removeClass('nav-up').addClass('nav-down');
            }
        }

        lastScrollTop = st;
    }

    // slider
    $('[data-slider]').each(function() {
        let $slider = $(this);
        let $tabs = $('.slider-tabs-mob__item');

        $slider.slick({
            slidesToShow: 3,
            dots: false,
            variableWidth: false,
            infinite: false,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        dots: true,
                        arrows: false
                    }
                }
            ]
        });

        $tabs.on('click', function(e) {
            let $this = $(this);
            let index = $this.index();

            console.log(index)

            $slider.slick('slickGoTo', index);
        });

        $slider.on('afterChange', function(event, slick, currentSlide, nextSlide) {
            console.log(currentSlide)

            $tabs
                .removeClass(active)
                .eq(currentSlide)
                .addClass(active);
        });
    });

    // Custom select
    let $select = $('[data-custom-select]');

    $select.selectmenu();

    // Инициализация форм
    webshim.setOptions('waitReady', false);
    webshim.setOptions('forms', {
        lazyCustomMessages: true,
        replaceValidationUI: true,
        addValidators: true
    });

    webshim.polyfill('forms forms-ext');

    // Форматирование цены
    function int2price(sum) {
        return sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }

    //
    $('[data-ui-slider]').each(function() {
        let $slider = $(this);
        let maxVal  = $slider.data('max-range');
        let minVal  = $slider.data('min-range');
        let step    = $slider.data('step');
        let $input  = $('#calculator-input')
        let value   = Number($input.val().replace(/\D/g,'').substring(0,8));

        $slider.slider({
            range : 'min',
            max   : maxVal,
            min   : minVal,
            value : value,
            step  : step,
            slide : function(e, ui) {
                let value = ui.value;

                $input.val(int2price(value));
                setCoefficients(value);
            }
        });

        $input.on('input', function() {
            let $this = $(this);
            let value = Number($this.val().replace(/\D/g,'').substring(0,8));

            if (value > maxVal) value = maxVal;

            if (value <= 0) {
                $this.val('');

                return;
            }

            $this.val(int2price(value));
            setCoefficients(value);
        });

        function setCoefficients(value) {
            let $bonus = $('[data-bonus]');
            let scores = value * 0.01;

            $bonus.each(function() {
                let $this = $(this);
                let k     = $this.data('bonus');
                let res   = int2price(Math.floor(scores * k));

                $this.text(res);
            });
        }
    });

    $('[data-modal]').fancybox({
        padding: 0,
        btnTpl : {
            smallBtn : '<div data-fancybox-close class="popup-form__close"></div>',
        },
        helpers: {
            overlay: {
                locked: false
            }
        }
    });

    $('[data-phone]').each(function() {
        var $this = $(this);

        $this.inputmask('+7 (999) 999-99-99', {
            showMaskOnHover: false
        });
    });

    $('[data-placeholder]').on('focus', function() {
        let $this = $(this);

        $this.on('keyup', function(e) {
            let $this = $(this);

            if ($this.val() != '') {
                $this.addClass('_focus');
            } else {
                $this.removeClass('_focus');
            }

            if (e.keyCode == '13') {
                $this.blur().removeClass('_focus');

                return;
            }
        });
    });

    $('[data-arr-up]').on('click', function() {
        $('body, html').animate({
            scrollTop : 0
        }, 500);
    });

    $('[data-categories]').on('click', function(e) {
        e.preventDefault();

        $body.addClass(categories);
    });

    $('[data-categories-close]').on('click', function(e) {
        $body.removeClass(categories);
    });

    $('[data-show-more]').on('click', function(e) {
        let $this = $(this);
        let type  = $this.data('show-more');
        let url   = (type === 'partners') ? 'partners.php' : 'news.php';

        console.log(url);

        e.preventDefault();

        $this.addClass(active);

        $.ajax({
            url: url,
            type: 'POST',
            data: '',
            dataType: 'json',
            success: function() {
                console.log('success');
            },
            complete: function(e) {
                // $this.removeClass(active);
            }
        });
    });
});
